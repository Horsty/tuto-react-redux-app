import { ADD_ARTICLE } from "../constants/action-types";
import { alertForbiddenWords } from "../actions";

const forbiddenWords = ["spam", "money"];

export function forbiddenWordsMiddleware({ dispatch }) {
  return function (next) {
    return function (action) {
      // do your stuff
      if (action.type === ADD_ARTICLE) {

        const foundWord = forbiddenWords.filter(word =>
          action.payload.title.includes(word)
        );

        if (foundWord.length) {
          return dispatch(alertForbiddenWords({ title: "Mot interdit" }));
        }
      }
      return next(action);
    };
  };
}