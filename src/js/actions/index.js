import { ADD_ARTICLE, FOUND_BAD_WORD, DATA_LOADED, DATA_REQUESTED } from "../constants/action-types";

export function addArticle(payload) {
  return { type: ADD_ARTICLE, payload }
};

export function alertForbiddenWords(payload) {
  return { type: FOUND_BAD_WORD, payload }
}

export function getData() {
  return { type: DATA_REQUESTED };
}